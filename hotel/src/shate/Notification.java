package shate;

public class Notification {



    private int id;
    private User user;
    private String message;
    private NotificationStatus status;


    public Notification(User user, String message, NotificationStatus status) {
        this.user = user;
        this.message = message;
        this.status = status;
    }

    public Notification(int id, User user, String message, NotificationStatus status) {
        this.id = id;
        this.user = user;
        this.message = message;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public NotificationStatus getStatus() {
        return status;
    }

    public void setStatus(NotificationStatus status) {
        this.status = status;
    }

}
