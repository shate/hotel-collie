package shate;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@WebServlet("/ChangeReservationsServlet")
public class ChangeReservationsServlet extends HttpServlet {

    private DBUtilAdmin dbUtilAdmin;
    private DBUtilUser dbUtilUser;
    private final String db_url = "jdbc:mysql://localhost:3306/ibgsm?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=CET";

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        try {
            dbUtilAdmin = new DBUtilAdmin();
            dbUtilUser = new DBUtilUser();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("CHANGING RESERVATIONS");
        RequestDispatcher dispatcher = request.getRequestDispatcher("change_reservations.jsp");


        List<Reservation> reservations = null;
        try {
            reservations = dbUtilAdmin.getReservations();
        } catch (Exception e) {
            e.printStackTrace();
        }
        request.setAttribute("RESERVATIONS", reservations);
        dispatcher.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        try {
            String command = request.getParameter("command");
            if (command == null) {
                command = "LIST";
            }
            System.out.println(command + "     < ====== COMMAND");

            switch (command) {
                case "ADD":
                    addReservation(request, response);
                    break;
                case "LOAD":
                    loadReservation(request, response);
                    break;
                case "UPDATE":
                    updateReservation(request, response);
                    break;
                case "DELETE":
                    deleteReservation(request, response);
                    break;
                default:
                    listReservations(request, response);
            }
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    private void addReservation(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        int id = Integer.parseInt(request.getParameter("RESERVATION_ID"));
        String roomType = request.getParameter("ROOM_TYPE");
        String userPESEL = request.getParameter("USER_PESEL");
        LocalDate startDate = LocalDate.parse(request.getParameter("START_DATE"), dateFormat);
        LocalDate endDate = LocalDate.parse(request.getParameter("END_DATE"), dateFormat);


        User user = dbUtilUser.getUser(userPESEL);
        Room room = new Room(roomType);

        Reservation reservation = new Reservation(id, user, room, startDate, endDate);

        dbUtilAdmin.addReservation(reservation);

        listReservations(request, response);
    }

    private void loadReservation(HttpServletRequest request, HttpServletResponse response) throws Exception {
        int id = Integer.parseInt(request.getParameter("RESERVATION_ID"));
        Reservation reservation = dbUtilAdmin.getReservation(id);
        request.setAttribute("RESERVATION", reservation);
        String message = (String) request.getAttribute("MESSAGE");
        if(message != null){
            request.setAttribute("MESSAGE", message);
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("update_reservation.jsp");
        dispatcher.forward(request, response);
    }

    private void updateReservation(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        int id = Integer.parseInt(request.getParameter("RESERVATION_ID"));
        String roomType = request.getParameter("ROOM_TYPE");
        String userPESEL = request.getParameter("USER_PESEL");
        LocalDate startDate = LocalDate.parse(request.getParameter("START_DATE"), dateFormat);
        LocalDate endDate = LocalDate.parse(request.getParameter("END_DATE"), dateFormat);

        User user = dbUtilUser.getUser(userPESEL);
        Room room = new Room(roomType);

        Reservation reservation = new Reservation(id, user, room, startDate, endDate);
        if (dbUtilUser.exists(userPESEL)){
            dbUtilAdmin.updateReservation(reservation);
            listReservations(request, response);
        }else{
            request.setAttribute("MESSAGE", String.format("User with <strong>%s</strong> PESEL doesn't exist", userPESEL));
            loadReservation(request, response);
        }
    }

    private void deleteReservation(HttpServletRequest request, HttpServletResponse response) throws Exception {
        int id = Integer.parseInt(request.getParameter("RESERVATION_ID"));
        dbUtilAdmin.deleteReservation(id);

        listReservations(request, response);
    }

    private void listReservations(HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<Reservation> reservations = dbUtilAdmin.getReservations();
        request.setAttribute("RESERVATIONS", reservations);
        String message = (String) request.getAttribute("MESSAGE");

        RequestDispatcher dispatcher = request.getRequestDispatcher("change_reservations.jsp");
        dispatcher.forward(request, response);
    }
}
