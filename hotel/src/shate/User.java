package shate;

public class User {
    private int id;
    private String firstName;
    private String surname;
    private String pesel;
    private String email;
    private String username;
    private String password;
    private String role;

    public User() {
    }

    public User(int id, String firstName, String surname, String pesel, String email, String username, String password, String role) {
        this.id = id;
        this.firstName = firstName;
        this.surname = surname;
        this.pesel = pesel;
        this.email = email;
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public User(String firstName, String surname, String pesel, String email, String username, String password, String role) {
        this.firstName = firstName;
        this.surname = surname;
        this.pesel = pesel;
        this.email = email;
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", surname='" + surname + '\'' +
                ", pesel='" + pesel + '\'' +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", role='" + role + '\'' +
                '}';
    }
}
