package shate;

public class HotelInfo {
    private double meanPrice;
    private int totalRooms;
    private int takenRooms;
    private int analyzedDays;

    public HotelInfo(double meanPrice, int totalRooms, int takenRooms, int analyzedDays) {
        this.meanPrice = meanPrice;
        this.totalRooms = totalRooms;
        this.takenRooms = takenRooms;
        this.analyzedDays = analyzedDays;
    }

    public double getMeanPrice() {
        return meanPrice;
    }
    public void setMeanPrice(double meanPrice) {
        this.meanPrice = meanPrice;
    }
    public int getTotalRooms() {
        return totalRooms;
    }
    public void setTotalRooms(int totalRooms) {
        this.totalRooms = totalRooms;
    }
    public int getTakenRooms() {
        return takenRooms;
    }
    public void setTakenRooms(int takenRooms) {
        this.takenRooms = takenRooms;
    }
    public int getAnalyzedDays() {
        return analyzedDays;
    }
    public void setAnalyzedDays(int analyzedDays) {
        this.analyzedDays = analyzedDays;
    }

// calkowity dochod jest liczony jako meanPrice * takenRooms
    public double calculateRevPAR(){
        return meanPrice * takenRooms *  analyzedDays / totalRooms;
    }

    public double calculateTRevPAR(){
        return 0.92 * meanPrice * takenRooms *analyzedDays / totalRooms;
    }

    public double calculateRevPOR(){
        return 0.92 * meanPrice * takenRooms * analyzedDays / takenRooms;
    }

}
