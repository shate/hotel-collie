package shate;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.List;

public abstract class DBUtil {

    protected static String getRole(DataSource dataSource, String name, String password){
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        String role = "BANNED";

        try {
            conn = dataSource.getConnection();
            //conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/hotelCollie?serverTimezone=CET", "root", "root_password");
            String sql = "SELECT role FROM users WHERE username=? and password=?";
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, password);

            resultSet = preparedStatement.executeQuery();

            while(resultSet.next())
                role = resultSet.getString("role");

        } catch(Exception e){
            e.printStackTrace();

        } finally {
            close(conn, preparedStatement, resultSet);
        }
        return role;
    }

    protected static void close(Connection conn, Statement statement, ResultSet resultSet) {

        try {

            if (resultSet != null)
                resultSet.close();

            if (statement != null)
                statement.close();

            if (conn != null)
                conn.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    protected static DataSource getAdminDataSource(){
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Context initContext;
        DataSource dataSource = null;
        try {
            initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:comp/env");

            dataSource = (DataSource) envContext.lookup("jdbc/admin");
        } catch(NamingException e){
            e.printStackTrace();
        }
        return dataSource;
    }

    protected static DataSource getClientDataSource(){
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Context initContext;
        DataSource dataSource = null;
        try {
            initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:comp/env");

            dataSource = (DataSource) envContext.lookup("jdbc/client");
        } catch(NamingException e){
            e.printStackTrace();
        }
        return dataSource;
    }
}


