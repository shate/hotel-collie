package shate;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class DBUtilUser{
    private DataSource dataSource;



    public DBUtilUser() {
        this.dataSource = DBUtil.getAdminDataSource();
    }

    protected String getRole(String name, String password){
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        String role = "BANNED";

        try {
            conn = dataSource.getConnection();
            //conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/hotelCollie?serverTimezone=CET", "root", "root_password");
            System.out.println(name + "   --------   " + password);
            String sql = "SELECT role FROM users WHERE username=? and password=SHA1(?)";
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, password);

            resultSet = preparedStatement.executeQuery();

            while(resultSet.next())
                role = resultSet.getString("role");

        } catch(Exception e){
            e.printStackTrace();

        } finally {
            DBUtil.close(conn, preparedStatement, resultSet);
        }
        return role;
    }

    protected boolean exists(String pesel){
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        boolean exists = false;

        try {
            conn = dataSource.getConnection();

            String sql = "SELECT * FROM users WHERE pesel=?";
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, pesel);

            resultSet = preparedStatement.executeQuery();

            exists = resultSet.next();

        } catch(Exception e){
            e.printStackTrace();

        } finally {
            DBUtil.close(conn, preparedStatement, resultSet);
        }
        return exists;
    }

    protected void addUser(User user){
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            conn = dataSource.getConnection();

            String sql = "INSERT INTO users(first_name, surname, pesel, email, username, password, role) VALUES(?, ?, ?, ?, ?, SHA1(?), ?)";
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getSurname());
            preparedStatement.setString(3, user.getPesel());
            preparedStatement.setString(4, user.getEmail());
            preparedStatement.setString(5, user.getUsername());
            preparedStatement.setString(6, user.getPassword());
            preparedStatement.setString(7, user.getRole());

            preparedStatement.executeUpdate();
        } catch(Exception e){
            e.printStackTrace();
        } finally {
            DBUtil.close(conn, preparedStatement, resultSet);
        }
    }


    protected User getUser(String username, String password){
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        User user = null;

        try {
            conn = dataSource.getConnection();

            String sql = "SELECT id, first_name, surname, pesel, email, username, password, role FROM users WHERE username=? and password=SHA1(?)";
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);

            resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                user = new User();
                int id = resultSet.getInt("id");
                String firstName = resultSet.getString("first_name");
                String surname = resultSet.getString("surname");
                String pesel = resultSet.getString("pesel");
                String email = resultSet.getString("email");
                String role = resultSet.getString("role");

                user.setId(id);
                user.setFirstName(firstName);
                user.setSurname(surname);
                user.setPesel(pesel);
                user.setEmail(email);
                user.setRole(role);
                user.setUsername(username);
                user.setPassword(password);
            }
        } catch(Exception e){
            e.printStackTrace();
        } finally {
            DBUtil.close(conn, preparedStatement, resultSet);
        }
        return user;
    }

    protected User getUser(String pesel){
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        User user = new User();

        try {
            conn = dataSource.getConnection();

            String sql = "SELECT username, password, id, first_name, surname, pesel, email, username, password, role FROM users WHERE pesel=?";
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, pesel);

            resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                int id = resultSet.getInt("id");
                String firstName = resultSet.getString("first_name");
                String username = resultSet.getString("username");
                String password = resultSet.getString("password");
                String surname = resultSet.getString("surname");
                String email = resultSet.getString("email");
                String role = resultSet.getString("role");

                user.setId(id);
                user.setFirstName(firstName);
                user.setSurname(surname);
                user.setPesel(pesel);
                user.setEmail(email);
                user.setRole(role);
                user.setUsername(username);
                user.setPassword(password);
            }
        } catch(Exception e){
            e.printStackTrace();
        } finally {
            DBUtil.close(conn, preparedStatement, resultSet);
        }
        return user;
    }
}
