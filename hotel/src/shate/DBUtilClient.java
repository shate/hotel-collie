package shate;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class DBUtilClient extends DBUtil {

    private DataSource dataSource;

    public DBUtilClient() {
        this.dataSource = DBUtil.getClientDataSource();
    }

    public List<Reservation> getReservations(User user) throws Exception {

        List<Reservation> reservations = new ArrayList<>();

        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            conn = dataSource.getConnection();
            String sql = "SELECT res.id, room.type, res.start_date, res.end_date FROM reservations res JOIN rooms room ON res.roomID = room.id AND res.userID = ?";
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, user.getId());
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                String roomType = resultSet.getString("type");
                int id = resultSet.getInt("id");

                LocalDate startDate = resultSet.getDate("start_date").toLocalDate();
                LocalDate endDate = resultSet.getDate("end_date").toLocalDate();

                Room room = new Room(roomType);
                Reservation reservation = new Reservation(id, user, room, startDate, endDate);
                reservations.add(reservation);
            }
        } finally {
            close(conn, preparedStatement, resultSet);
        }
        return reservations;
    }

    public List<Notification> getNotifications(User user) throws SQLException {
        List<Notification> notifications = new ArrayList<>();

        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            conn = dataSource.getConnection();
            String sql = "SELECT * FROM notifications WHERE userID = ?";
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, user.getId());
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String message = resultSet.getString("message");
                String status = resultSet.getString("status");

                Notification notification = new Notification(id, user, message, NotificationStatus.valueOf(status));
                notifications.add(notification);
            }
        } finally {
            close(conn, preparedStatement, resultSet);
        }
        return notifications;
    }

    public void bookAccommodation(Reservation reservation) throws SQLException {
        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        int userID = reservation.getUser().getId();
        System.out.println(userID + "   BOOKED");
        int roomID = reservation.getRoom().getId();
        Date startDate = Date.valueOf(reservation.getStartDate());
        Date endDate = Date.valueOf(reservation.getEndDate());
        try {
            conn = dataSource.getConnection();
            String sql = "CALL book(?, ?, ?, ?);";
            statement = conn.prepareStatement(sql);

            statement.setInt(1, userID);
            statement.setInt(2, roomID);
            statement.setDate(3, startDate);
            statement.setDate(4, endDate);

            System.out.println(statement.toString());
            statement.executeQuery();
        } finally {
            close(conn, statement, resultSet);
        }
    }
}
