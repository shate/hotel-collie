package shate;

public enum NotificationStatus{NEW("NEW"), OPENED("OPENED"), DELETED("DELETED");
    private String status;

    NotificationStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
