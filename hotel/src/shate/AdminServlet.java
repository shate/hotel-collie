package shate;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/AdminServlet")
public class AdminServlet extends HttpServlet {
    private DBUtilAdmin dbUtil;
    private final String db_url = "jdbc:mysql://localhost:3306/ibgsm?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=CET";

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        try {
            dbUtil = new DBUtilAdmin();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/admin_view.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        RequestDispatcher dispatcher = request.getRequestDispatcher("admin_view.jsp");
        try {
            String action = request.getParameter("action");
            if (action.equals("CHANGE_ROOMS")) {
                dispatcher = request.getRequestDispatcher("change_rooms.jsp");
            } else if (action.equals("CHANGE_RESERVATIONS")) {
                dispatcher = request.getRequestDispatcher("ChangeReservationsServlet");
                List<Reservation> reservations = dbUtil.getReservations();
                request.setAttribute("RESERVATIONS", reservations);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        dispatcher.forward(request, response);
    }
}
