package shate;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@WebServlet("/BookAccommodationServlet")
public class BookAccommodationServlet extends HttpServlet {
    private DBUtilClient dbUtilClient;
    private final String db_url = "jdbc:mysql://localhost:3306/hotelcollie?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=CET";

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        try {
            dbUtilClient = new DBUtilClient();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate startDate = LocalDate.parse(request.getParameter("startDate"), dateFormat);
        LocalDate  endDate = LocalDate.parse(request.getParameter("endDate"), dateFormat);
        HttpSession httpSession  = request.getSession();

        User user = (User) httpSession.getAttribute("USER");
        Room room = new Room(request.getParameter("roomType"));
        System.out.println(room.getType());

        Reservation reservation = new Reservation(user, room, startDate, endDate);

        request.setAttribute("reservation", reservation);

        List<Notification> notifications = null;

        try {
            dbUtilClient.bookAccommodation(reservation);
            notifications = dbUtilClient.getNotifications(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        request.setAttribute("NOTIFICATIONS", notifications);
        request.getRequestDispatcher("client_view.jsp").forward(request, response);
    }
}
