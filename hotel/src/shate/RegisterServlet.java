package shate;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.io.IOException;

@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
    DBUtilUser dbUtilUser = new DBUtilUser();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        String firstName = request.getParameter("firstNameInput");
        String surname = request.getParameter("surnameInput");
        String pesel = request.getParameter("peselInput");
        String email = request.getParameter("emailInput");
        String username = request.getParameter("usernameInput");
        String password = request.getParameter("passwordInput");
        String confirmPassword = request.getParameter("confirmPasswordInput");

        RequestDispatcher dispatcher;
        boolean exists = dbUtilUser.exists(pesel);
        if(exists){
            String message = "This user already exists";
            request.setAttribute("message", message);
            dispatcher = request.getRequestDispatcher("register.jsp");
        }else if(password.equals(confirmPassword)){
            User user = new User(firstName, surname, pesel, email, username, password, "CLIENT");
            dbUtilUser.addUser(user);
            dispatcher = request.getRequestDispatcher("index.jsp");
        }else{
            String message = "Passwords does not match";
            request.setAttribute("message", message);
            dispatcher = request.getRequestDispatcher("register.jsp");
        }
        dispatcher.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }



}
