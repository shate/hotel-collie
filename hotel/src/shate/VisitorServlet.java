package shate;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/VisitorServlet")
public class VisitorServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
        try{
            String action = request.getParameter("action");
            if(action.equals("SEE_ROOMS")){
                dispatcher = request.getRequestDispatcher("visitor_rooms.jsp");
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        dispatcher.forward(request, response);
    }
}
