package shate;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet("/ClientServlet")
public class ClientServlet extends HttpServlet {
    DBUtilClient  dbUtilClient;

    @Override
    public void init() throws ServletException {
        super.init();

        try {
            dbUtilClient = new DBUtilClient();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession httpSession = request.getSession();

        List<Notification> notifications = null;
        try {
            notifications = dbUtilClient.getNotifications((User) httpSession.getAttribute("USER"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        request.setAttribute("NOTIFICATIONS", notifications);
        request.getRequestDispatcher("client_view.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        RequestDispatcher dispatcher = request.getRequestDispatcher("client_view.jsp");
        HttpSession httpSession = request.getSession();
        User user = (User) httpSession.getAttribute("USER");
        try{
            String action = request.getParameter("action");
            switch (action) {
                case "SEE_ROOMS":
                    dispatcher = request.getRequestDispatcher("client_rooms.jsp");
                    break;
                case "BOOK_ACCOMMODATION":
                    dispatcher = request.getRequestDispatcher("book_accommodation.jsp");
                    break;
                case "SEE_RESERVATIONS":
                    dispatcher = request.getRequestDispatcher("client_reservations.jsp");
                    List<Reservation> reservations = dbUtilClient.getReservations(user);
                    request.setAttribute("RESERVATIONS", reservations);
                    break;
                case "SEE_NOTIFICATIONS":
                    dispatcher = request.getRequestDispatcher("client_notifications.jsp");
                    break;
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        List<Notification> notifications = null;
        try {
            notifications = dbUtilClient.getNotifications(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        request.setAttribute("NOTIFICATIONS", notifications);
        dispatcher.forward(request, response);
    }
}
