package shate;

import java.util.HashMap;

public class Room {
    private int id;

    public int getId() {
        return id;
    }

    public Room(int id, String type, int capacity, double price, HashMap<String, String> amenities) {
        this.id = id;
        this.type = type;
        this.capacity = capacity;
        this.price = price;
        this.amenities = amenities;
    }

    private String type;
    private int capacity;
    private double price;
    private HashMap<String, String> amenities;

    public Room(String type) {
        this.type = type;
        getRoomInfo();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public HashMap<String, String> getAmenities() {
        return amenities;
    }

    public void setAmenities(HashMap<String, String> amenities) {
        this.amenities = amenities;
    }

    private void getRoomInfo(){
        int capacity = 0;
        double price = 0;
        HashMap<String, String> amenities = new HashMap<>();
        switch (type){
            case "base":
                capacity = 2;
                price = 100;
                amenities.put("Kitchen", "NO");
                amenities.put("Bathroom", "NO");
                amenities.put("Terrace", "NO");
                this.id = 1;
                break;
            case "comfort":
                capacity = 3;
                price = 200;
                amenities.put("Kitchen", "YES");
                amenities.put("Bathroom", "YES");
                amenities.put("Terrace", "NO");
                this.id = 2;
                break;
            case "premium":
                capacity = 2;
                price = 400;
                amenities.put("Kitchen", "YES");
                amenities.put("Bathroom", "YES");
                amenities.put("Terrace", "YES");
                this.id = 3;
                break;
        }
        this.capacity = capacity;
        this.price = price;
        this.amenities = amenities;
    }
}
