package shate;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Reservation {
    private int id;
    private User user;
    private Room room;
    private LocalDate startDate;
    private LocalDate endDate;
    private long days;

    public int getId() {
        return id;
    }

    public Reservation(int id, User user, Room room, LocalDate startDate, LocalDate endDate) {
        this.id = id;
        this.user = user;
        this.room = room;
        this.startDate = startDate;
        this.endDate = endDate;
        this.days = ChronoUnit.DAYS.between(startDate, endDate);
    }


    public Reservation( User user, Room room, LocalDate startDate, LocalDate endDate) {
        this.user = user;
        this.room = room;
        this.startDate = startDate;
        this.endDate = endDate;
        this.days = ChronoUnit.DAYS.between(startDate, endDate);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public long getDays() {
        return days;
    }

    public double calculatePrice(){
        return days * room.getPrice();
    }
}
