package shate;


import javax.management.relation.Role;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
    DBUtilUser dbUtilUser = new DBUtilUser();
    DBUtilClient dbUtilClient = new DBUtilClient();

    private final String db_url = "jdbc:mysql://localhost:3306/hotelcollie?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=CET";


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        HttpSession httpSession = request.getSession();
        String username = request.getParameter("loginInput");
        String password = request.getParameter("passwordInput");
        RequestDispatcher dispatcher = null;
        String role = null;
        User user = dbUtilUser.getUser(username, password);
        if(user != null){
            role = user.getRole();
            if(role.equals("ADMIN")){
                dispatcher = request.getRequestDispatcher("AdminServlet");
            }
            else if(role.equals("CLIENT")) {
                dispatcher = request.getRequestDispatcher("ClientServlet");
                List<Notification> notifications = new ArrayList<>();
                try {
                    notifications = dbUtilClient.getNotifications(user);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                request.setAttribute("NOTIFICATIONS", notifications);
            }
        }else{
            dispatcher = request.getRequestDispatcher("login.jsp");
            request.setAttribute("LOGIN_MESSAGE", "Wrong login or password");
        }

        httpSession.setAttribute("username", username);
        httpSession.setAttribute("USER", user);
        httpSession.setAttribute("password", password);
        dispatcher.include(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
