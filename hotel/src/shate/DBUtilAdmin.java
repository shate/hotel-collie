package shate;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class DBUtilAdmin extends DBUtil {

    private DataSource dataSource;
    private DBUtilUser dbUtilUser;

    public DBUtilAdmin() {
        this.dataSource = DBUtil.getAdminDataSource();
        this.dbUtilUser = new DBUtilUser();
    }

    public List<Reservation> getReservations() throws Exception {
        List<Reservation> reservations = new ArrayList<>();

        Connection conn = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            conn = dataSource.getConnection();

            String sql = "SELECT res.id, room.type, u.pesel, res.start_date, res.end_date" +
                    " FROM reservations res JOIN rooms room ON room.id = res.roomID JOIN users u ON res.userID = u.id";
            statement = conn.createStatement();

            resultSet = statement.executeQuery(sql);
            Reservation reservation;
            while (resultSet.next()) {

                int reservationID = resultSet.getInt("id");
                String roomType = resultSet.getString("type");
                String userPESEL = resultSet.getString("pesel");
                LocalDate startDate = resultSet.getDate("start_date").toLocalDate();
                LocalDate endDate = resultSet.getDate("end_date").toLocalDate();

                User user = dbUtilUser.getUser(userPESEL);
                Room room = new Room(roomType);
                reservation = new Reservation(reservationID, user, room, startDate, endDate);
                reservations.add(reservation);
            }
        } finally {
            close(conn, statement, resultSet);
        }

        return reservations;
    }

    public void addReservation(Reservation reservation) throws Exception {

        Connection conn = null;
        PreparedStatement preparedStatement = null;

        try {
            conn = dataSource.getConnection();

            String sql = "INSERT INTO reservations(userID, roomID, start_date, end_date) " +
                    "VALUES(?, ?, ?, ?)";


            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, reservation.getUser().getId());
            preparedStatement.setInt(2, reservation.getRoom().getId());
            preparedStatement.setDate(3, Date.valueOf(reservation.getStartDate()));
            preparedStatement.setDate(4, Date.valueOf(reservation.getEndDate()));

            preparedStatement.executeUpdate();
        } finally {
            close(conn, preparedStatement, null);
        }
    }

    public Reservation getReservation(int id) throws Exception {

        Reservation reservation = null;

        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            conn = dataSource.getConnection();

            String sql = "SELECT u.pesel, room.type, res.start_date, res.end_date" +
            " FROM reservations res JOIN rooms room ON room.id = res.roomID JOIN users u ON res.userID = u.id WHERE res.id = ?";

            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, id);

            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                String pesel = resultSet.getString("pesel");
                String roomType = resultSet.getString("type");
                LocalDate startDate = resultSet.getDate("start_date").toLocalDate();
                LocalDate endDate = resultSet.getDate("end_date").toLocalDate();
                User user = dbUtilUser.getUser(pesel);
                Room room = new Room(roomType);

                reservation = new Reservation(id, user, room, startDate, endDate);

            } else {
                throw new Exception("There is no reservation with id = " + id);
            }
        } finally {
            close(conn, preparedStatement, resultSet);
        }
        return reservation;
    }

    public void updateReservation(Reservation reservation) throws Exception {

        Connection conn = null;
        PreparedStatement preparedStatement = null;
        PreparedStatement preparedStatement2 = null;

        try {

            conn = dataSource.getConnection();
            String sql = "UPDATE reservations SET userID = ?, roomID = ?, start_date = ?, end_date = ? WHERE id = ?";

            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, reservation.getUser().getId());
            preparedStatement.setInt(2, reservation.getRoom().getId());
            preparedStatement.setDate(3, Date.valueOf(reservation.getStartDate()));
            preparedStatement.setDate(4, Date.valueOf(reservation.getEndDate()));
            preparedStatement.setInt(5, reservation.getId());
            preparedStatement.executeUpdate();

            String sql2 = "INSERT INTO notifications (userID, message, status) VALUES (?, ?, ?);";
            preparedStatement2 = conn.prepareStatement(sql2);
            preparedStatement2.setInt(1, reservation.getUser().getId());
            String message = String.format("Your reservation has been updated:\nStart date: %s\nEnd date: %s\nRoom type: %s",
                    reservation.getStartDate().toString(),
                    reservation.getEndDate().toString(),
                    reservation.getRoom().getType());
            preparedStatement2.setString(2, message);
            preparedStatement2.setString(3, "NEW");
            preparedStatement2.executeUpdate();

        } finally {
            close(conn, preparedStatement, null);
        }
    }

    public void deleteReservation(int id) throws Exception {

        Connection conn = null;
        PreparedStatement preparedStatement = null;
        PreparedStatement preparedStatement2 = null;

        try {
            conn = dataSource.getConnection();

            String sql = "DELETE FROM reservations WHERE id =?";

            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, id);

            String sql2 = "INSERT INTO notifications (userID, message, status) VALUES (?, ?, ?);";
            preparedStatement2 = conn.prepareStatement(sql2);
            preparedStatement2.setInt(1, getReservation(id).getUser().getId());
            Reservation reservation = getReservation(id);
            String message = String.format("Your reservation has been deleted (Start date: %s,   End date: %s,    Room type: %s)",
                    reservation.getStartDate(),
                    reservation.getEndDate(),
                    reservation.getRoom().getType());
            preparedStatement2.setString(2, message);
            preparedStatement2.setString(3, "NEW");
            preparedStatement2.executeUpdate();

            preparedStatement.executeUpdate();
        } finally {
            close(conn, preparedStatement, null);
        }
    }
}
