<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Hotel Collie</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/jpg" href="img/icon.jpg">

    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">

</head>

<body>
<div class="main-page">
<nav class="navbar navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar">HOTEL COLLIE</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand glyphicon glyphicon-home" id="selected" href="#"></a>
            <c:url var = "changeRooms" value="AdminServlet">
                <c:param name="action" value="CHANGE_ROOMS"></c:param>
            </c:url>
            <c:url var = "reservations" value="ChangeReservationsServlet">
                <c:param name="action" value="CHANGE_RESERVATIONS"></c:param>
            </c:url>
            <a class="navbar-brand" href="${changeRooms}">Rooms</a>
            <a class="navbar-brand" href="change_contact.jsp">Contact</a>
            <a class="navbar-brand" href="${reservations}">Reservations</a>
            <a class="navbar-brand" href="LogoutServlet">Log out</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse navbar-right">
            <p class="navbar-brand">username: ${USER.username}  ,   role: ${USER.role}</p>
        </div>
    </div>
</nav>

<div class="jumbotron">
    <h1>Hotel Collie</h1>

    <div class="container">


        <div class="bg-text">
            <p>This is the best place to forget about your everyday life!</p>
        </div>
    </div>
</div>
</div>
</body>
</html>
