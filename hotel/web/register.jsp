<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Admin log panel</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/main.css">
    </head>


    <body>

        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar">HOTEL COLLIE</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <c:url var = "seeRooms" value="VisitorServlet">
                        <c:param name="action" value="SEE_ROOMS"></c:param>
                    </c:url>
                    <a class="navbar-brand glyphicon glyphicon-home" href="index.jsp"></a>
                    <a class="navbar-brand" href="${seeRooms}">Rooms</a>
                    <a class="navbar-brand" href="contact.jsp">Contact</a>
                    <a class="navbar-brand" href="login.jsp">Log in</a>
                    <a class="navbar-brand" id="selected" href="register.jsp">Register</a>
                </div>
            </div>
        </nav>

        <div class="row form-group"></div>
        <div class="row form-group"></div>
        <div class="row form-group"></div>

        <div class="jumbotron">
            <h1>Register panel</h1>
            <form class="formulas" action="RegisterServlet" method="post">
                <div class="form-group">
                    <label for="firstName">First name</label>
                    <input id="firstName" type="text" class="form-control" name="firstNameInput" placeholder="Enter first name here">
                    <label for="surname">Surname</label>
                    <input id="surname" class="form-control" name="surnameInput" placeholder="Enter surname here">
                    <label for="pesel">PESEL</label>
                    <input id="pesel" class="form-control" name="peselInput" placeholder="Enter PESEL here">
                    <label for="email">E - mail</label>
                    <input id="email" class="form-control" type="email" name="emailInput" placeholder="Enter e-mail here">
                    <label for="username">Username</label>
                    <input id="username" class="form-control" name="usernameInput" placeholder="Enter username here">
                    <small id="emailHelp" class="form-text text-muted">${message}</small>
                    <label for="password">Password</label>
                    <input id="password" class="form-control" type="password" name="passwordInput" placeholder="Enter password here">
                    <label for="confirmPassword">Confirm password</label>
                    <input id="confirmPassword" class="form-control" type="password" name="confirmPasswordInput" placeholder="Confirm password here">
                </div>

                <button type="submit" class="btn btn-primary">Register</button>
            </form>
        </div>

        <div class="row form-group"></div>
        <div class="row form-group"></div>
        <div class="row form-group"></div>

        </body>
    </html>