<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"
         import="java.util.*, shate.Reservation" %>
<html>
    <head>
        <title>Zmiana danych telefonu</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/main.css">
    </head>

    <body>

        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar">HOTEL COLLIE</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand glyphicon glyphicon-home" href="admin_view.jsp"></a>
                    <c:url var = "changeRooms" value="AdminServlet">
                        <c:param name="action" value="CHANGE_ROOMS"></c:param>
                    </c:url>
                    <c:url var = "reservations" value="AdminServlet">
                        <c:param name="action" value="CHANGE_RESERVATIONS"></c:param>
                    </c:url>
                    <a class="navbar-brand" href="${changeRooms}">Rooms</a>
                    <a class="navbar-brand" href="change_contact.jsp" role="button">Contact</a>
                    <a class="navbar-brand" id="selected" href="${reservations}">Reservations</a>
                    <a class="navbar-brand" href="LogoutServlet" role="button">Log out</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse navbar-right">
                    <p class="navbar-brand">username: ${USER.username}  ,   role: ${USER.role}</p>
                </div>
            </div>
        </nav>

        <div class="row form-group"></div>
        <div class="row form-group"></div>
        <div class="row form-group"></div>

        <div class="jumbotron">
            <div class="container">
                <h1>Change reservation data</h1>

                <form class="formulas" action="ChangeReservationsServlet" method="get">
                    <input type="hidden" name="command" value="UPDATE"/>
                    <input type="hidden" name="RESERVATION_ID" value="${RESERVATION.id}"/>
                    <div class="form-group">
                        <label for="roomType">Room type</label>
                        <input id="roomType" type="text" class="form-control" name="ROOM_TYPE" value="${RESERVATION.room.type}"/>
                        <label for="pesel">PESEL</label>
                        <input id="pesel" type="text" class="form-control" name="USER_PESEL" value="${RESERVATION.user.pesel}"/>
                        <small style="color: darkred" id="emailHelp" class="form-text text-muted">${MESSAGE}</small><br>
                        <label for="startDate">Start date</label>
                        <input id="startDate" type="date" class="form-control" name="START_DATE" value="${RESERVATION.startDate}"/>
                        <label for="endDate">End date</label>
                        <input id="endDate" type="date" class="form-control" name="END_DATE" value="${RESERVATION.endDate}"/>
                    </div>
                    <button type="submit" class="btn btn-success">Change</button>
                </form>
            </div>
        </div>

        <div class="row form-group"></div>
        <div class="row form-group"></div>
    </body>
</html>

<script>
    const startDate = document.getElementById("startDate");
    startDate.addEventListener('change', (event) =>{
        document.getElementById("endDate").setAttribute("min", startDate.value);
    });
</script>
