<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<%@ page contentType="text/html;charset=UTF-8" language="java"
         import="shate.HotelInfo, java.util.*" %>

<%--sample data--%>
<%

  String meanPrice = request.getParameter("meanPrice");
  String totalRooms = request.getParameter("totalRooms");
  String takenRooms = request.getParameter("takenRooms");
  String analyzedDays = request.getParameter("analyzedDays");
  String metricType = request.getParameter("metricType");
  pageContext.setAttribute("metricType", metricType);
  HotelInfo hotelInfo = new HotelInfo(Double.parseDouble(meanPrice), Integer.parseInt(totalRooms), Integer.parseInt(takenRooms), Integer.parseInt(analyzedDays));

%>

<html>
<head>
  <title>JSTL test</title>
  <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<c:set var="hotel" value="<%= hotelInfo %>"/>



<c:if test="${metricType.equals('RevPAR')}">
  RevPAR metric: <fmt:formatNumber type="number" maxFractionDigits="2" value="${hotel.calculateRevPAR()}"/>
</c:if>

<c:if test="${metricType.equals('TRevPAR')}">
  TRevPAR metric: <fmt:formatNumber type="number" maxFractionDigits="2" value="${hotel.calculateTRevPAR()}"/>
</c:if>

<c:if test="${metricType.equals('RevPOR')}">
  RevPOR metric: <fmt:formatNumber type="number" maxFractionDigits="2" value="${hotel.calculateRevPOR()}"/>
</c:if>

</body>
</html>
