<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <meta charset="utf-8">
        <title>Hotel Collie</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" type="image/jpg" href="img/icon.jpg">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="css/main.css">
    </head>
    <body>
        <nav class="navbar navbar-inverse  navbar-fixed-top text-center" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar">HOTEL COLLIE</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <c:url var = "seeRooms" value="VisitorServlet">
                        <c:param name="action" value="SEE_ROOMS"></c:param>
                    </c:url>
                    <a class="navbar-brand glyphicon glyphicon-home" href="index.jsp"></a>
                    <a class="navbar-brand" href="${seeRooms}">Rooms</a>
                    <a class="navbar-brand" id="selected" href="contact.jsp">Contact</a>
                    <a class="navbar-brand" href="login.jsp">Log in</a>
                    <a class="navbar-brand" href="register.jsp">Register</a>
                </div>
            </div>
        </nav>
        <div class="contact">
            <div class="container-fluid">
                <div class="heading">
                    <h2 class="content-title">Did you decide?</h2>
                    <p class="content-description">Regardless - recommend us to your friends!</p>

                </div>
                <div class="span8">
                    <div style="width: 100%">
                        <iframe width="100%" height="600" src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;q=plac%20orl%C4%85t%20lwowskich+(My%20Business%20Name)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
                            <a href="https://www.maps.ie/coordinates.html">latitude longitude finder</a>
                        </iframe>
                    </div>
                    <br />
                </div>

            </div>
            <address class="contact-info">Trip ranking<br> pl. Orląt Lwowskich <br> 99-999 Wrocław <br>
                <span class="glyphicon glyphicon-envelope"></span>
                trip@.com.pl </address>

            </div>
        </div>

    </body>
</html>
