<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Hotel Collie</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/jpg" href="img/icon.jpg">

        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="css/main.css">
    </head>

    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar">HOTEL COLLIE</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <c:url var = "seeRooms" value="ClientServlet">
                        <c:param name="action" value="SEE_ROOMS"></c:param>
                    </c:url>
                    <c:url var = "bookAccommodation" value="ClientServlet">
                        <c:param name="action" value="BOOK_ACCOMMODATION"></c:param>
                    </c:url>
                    <c:url var = "seeReservations" value="ClientServlet">
                        <c:param name="action" value="SEE_RESERVATIONS"></c:param>
                    </c:url>

                    <a class="navbar-brand glyphicon glyphicon-home" href="client_view.jsp"></a>
                    <a class="navbar-brand" href="${seeRooms}">Rooms</a>
                    <a class="navbar-brand" href="${bookAccommodation}">Book accommodation</a>
                    <a class="navbar-brand" href="${seeReservations}">See reservations</a>
                    <c:url var = "seeNotifications" value="ClientServlet">
                        <c:param name="action" value="SEE_NOTIFICATIONS"></c:param>
                    </c:url>
                    <a class="navbar-brand" id="selected" href="${seeNotifications}">Notifications [${NOTIFICATIONS.size()}]</a>
                    <a class="navbar-brand" href="LogoutServlet">Log out</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse navbar-right">
                    <p class="navbar-brand">username: ${USER.username}  ,   role: ${USER.role}</p>
                </div>
            </div>
        </nav>



        <div class="main-content">
            <h1>Reservations</h1>

            <div class="row form-group"></div>
            <div class="row form-group"></div>
            <div class="row form-group"></div>

            <table class="table table-striped">

                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Message</th>
                    <th scope="col">Status</th>
                </tr>
                </thead>
                <tbody>

                <c:forEach var="notification" items="${NOTIFICATIONS}">
                    <tr>
                        <th scope="row">${notification.id}</th>
                        <td>${notification.message}</td>
                        <td>${notification.status}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </body>
</html>
