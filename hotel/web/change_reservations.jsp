<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"
         import="java.util.*, shate.Reservation" %>



<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Hotel Collie</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/jpg" href="img/icon.jpg">

        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="css/main.css">

    </head>

    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar">HOTEL COLLIE</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand glyphicon glyphicon-home" href="admin_view.jsp"></a>
                    <c:url var = "changeRooms" value="AdminServlet">
                        <c:param name="action" value="CHANGE_ROOMS"></c:param>
                    </c:url>
                    <c:url var = "reservations" value="ChangeReservationsServlet">
                        <c:param name="action" value="CHANGE_RESERVATIONS"></c:param>
                    </c:url>
                    <a class="navbar-brand" href="${changeRooms}">Rooms</a>
                    <a class="navbar-brand" href="change_contact.jsp">Contact</a>
                    <a class="navbar-brand" id="selected" href="${reservations}">Reservations</a>
                    <a class="navbar-brand" href="LogoutServlet">Log out</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse navbar-right">
                    <p class="navbar-brand">username: ${USER.username}  ,   role: ${USER.role}</p>
                </div>
            </div>
        </nav>


        <div class="jumbotron">
            <h1>Reservations</h1>

            <div class="row form-group"></div>
            <div class="row form-group"></div>
            <div class="row form-group"></div>

            <table class="table table-striped" >

                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Room type</th>
                    <th scope="col">User PESEL</th>
                    <th scope="col">Start date</th>
                    <th scope="col">End date</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>

                <c:forEach var="reservation" items="${RESERVATIONS}">

                    <c:url var="updateLink" value="ChangeReservationsServlet">
                        <c:param name="command" value="LOAD"></c:param>
                        <c:param name="RESERVATION_ID" value="${reservation.id}"></c:param>
                    </c:url>

                    <c:url var="deleteLink" value="ChangeReservationsServlet">
                        <c:param name="command" value="DELETE"></c:param>
                        <c:param name="RESERVATION_ID" value="${reservation.id}"></c:param>
                    </c:url>


                    <tr class="highlight">
                        <th scope="row">${reservation.id}</th>
                        <td>${reservation.room.type}</td>
                        <td>${reservation.user.pesel}</td>
                        <td>${reservation.startDate}</td>
                        <td>${reservation.endDate}</td>

                        <td><a href="${updateLink}">
                            <button type="button" class="btn btn-success">Change</button>
                        </a>
                            <a href="${deleteLink}"
                               onclick="if(!(confirm('Are you sure, that you want to cancel that reservation?'))) return false">
                                <button type="button" class="btn btn-danger">Delete</button>
                            </a></td>
                    </tr>

                </c:forEach>
                </tbody>
            </table>
        </div>
    </body>
</html>
