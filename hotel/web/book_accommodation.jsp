<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Hotel Collie</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/jpg" href="img/icon.jpg">

        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="css/main.css">

        <style>
            .user-values{
                font-size: 24px;
                font-weight: bold;
            }
        </style>
    </head>

    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar">HOTEL COLLIE</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <c:url var = "seeRooms" value="ClientServlet">
                        <c:param name="action" value="SEE_ROOMS"></c:param>
                    </c:url>
                    <c:url var = "bookAccommodation" value="ClientServlet">
                        <c:param name="action" value="BOOK_ACCOMMODATION"></c:param>
                    </c:url>
                    <c:url var = "seeReservations" value="ClientServlet">
                        <c:param name="action" value="SEE_RESERVATIONS"></c:param>
                    </c:url>

                    <a class="navbar-brand glyphicon glyphicon-home" href="client_view.jsp"></a>
                    <a class="navbar-brand" href="${seeRooms}">Rooms</a>
                    <a class="navbar-brand" id="selected" href="${bookAccommodation}">Book accommodation</a>
                    <a class="navbar-brand" href="${seeReservations}">See reservations</a>
                    <c:url var = "seeNotifications" value="ClientServlet">
                        <c:param name="action" value="SEE_NOTIFICATIONS"></c:param>
                    </c:url>
                    <a class="navbar-brand" href="${seeNotifications}">Notifications [${NOTIFICATIONS.size()}]</a>
                    <a class="navbar-brand" href="LogoutServlet">Log out</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse navbar-right">
                    <p class="navbar-brand">username: ${USER.username}  ,   role: ${USER.role}</p>
                </div>
            </div>
        </nav>

        <div class="main-content">
            <form action="BookAccommodationServlet" method="get">

                <div class="form-group">
                    <label for="firstName">First name</label>
                    <div class="user-values" id="firstName">${USER.firstName}</div>
                </div>

                <div class="form-group">
                    <label for="surname">Surname</label>
                    <div class="user-values" id="surname">${USER.surname}</div>
                </div>

                <div class="form-group">
                    <label for="email">E-mail</label>
                    <div class="user-values" id="email">${USER.email}</div>
                </div>

                <div class="form-group">
                    <label for="startDateID">Start Date</label>
                    <input type="date" id="startDateID" name="startDate" placeholder="2020-04-26" value="2020-04-26">
                </div>

                <div class="form-group">
                <label for="endDateID">End Date</label>
                    <input type="date" id="endDateID" name="endDate" placeholder="2020-04-28" value="2020-04-28" min="2020-04-26">
                </div>

                <div class="form-group">
                    <label for="roomTypes">Room type:</label>
                    <select class="form-control" id="roomTypes" name="roomType">
                        <option value="base">Base</option>
                        <option value="comfort">Comfort</option>
                        <option value="premium">Premium</option>
                    </select>
                </div>
                <input type="submit" value="Submit" class="btn btn-default">
            </form>
        </div>
    </body>
</html>

<script>
    const startDate = document.getElementById("startDateID");
    startDate.addEventListener('change', (event) =>{
        document.getElementById("endDateID").setAttribute("min", startDate.value);
    });
</script>
