<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Log panel</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/main.css">
    </head>

    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar">HOTEL COLLIE</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <c:url var = "seeRooms" value="VisitorServlet">
                        <c:param name="action" value="SEE_ROOMS"></c:param>
                    </c:url>
                    <a class="navbar-brand glyphicon glyphicon-home" href="index.jsp"></a>
                    <a class="navbar-brand" href="${seeRooms}">Rooms</a>
                    <a class="navbar-brand" href="contact.jsp">Contact</a>
                    <a class="navbar-brand" id="selected" href="login.jsp">Log in</a>
                    <a class="navbar-brand" href="register.jsp">Register</a>
                </div>
            </div>
        </nav>

        <div class="row form-group"></div>
        <div class="row form-group"></div>
        <div class="row form-group"></div>

        <div class="jumbotron">
            <h1>Logging in panel</h1>
            <form class="formulas" action="LoginServlet" method="post">
                <div class="form-group">
                    <label for="login">Login</label>
                    <input id="login" type="text" class="form-control" name="loginInput" placeholder="Enter login here">
                    <label for="passwordID">Password</label>
                    <input id="passwordID" type="password" class="form-control" name="passwordInput" placeholder="Enter password here">
                    <small style="color: darkred" id="emailHelp" class="form-text text-muted">${LOGIN_MESSAGE}</small><br>

                </div>
                <button type="submit" class="btn btn-primary">Log in</button>
            </form>
        </div>

        <div class="row form-group"></div>
        <div class="row form-group"></div>
        <div class="row form-group"></div>

    </body>
</html>
