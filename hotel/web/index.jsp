<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html class="no-js" lang="">
  <head>
    <meta charset="utf-8">
    <title>Hotel Collie</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/jpg" href="img/icon.jpg">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
  </head>

  <body>
    <div class="main-page">
      <nav class="navbar navbar-fixed-top text-center" role="navigation">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar">HOTEL COLLIE</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <c:url var = "seeRooms" value="VisitorServlet">
              <c:param name="action" value="SEE_ROOMS"></c:param>
            </c:url>
            <a class="navbar-brand glyphicon glyphicon-home" id="selected" href="#"></a>
            <a class="navbar-brand" href="${seeRooms}">Rooms</a>
            <a class="navbar-brand" href="contact.jsp">Contact</a>
            <a class="navbar-brand" href="login.jsp">Log in</a>
            <a class="navbar-brand" href="register.jsp">Register</a>
          </div>
         </div>
      </nav>

      <div class="jumbotron">
        <h1>Hotel Collie</h1>
        <div class="container">
          <div class="bg-text">
            <p>This is the best place to forget about your everyday life!</p>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
