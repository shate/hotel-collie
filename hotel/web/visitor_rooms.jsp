<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <title>Rooms</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" type="image/jpg" href="img/icon.jpg">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="css/main.css">

    </head>

    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar">HOTEL COLLIE</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <c:url var = "seeRooms" value="ClientServlet">
                        <c:param name="action" value="SEE_ROOMS"></c:param>
                    </c:url>
                    <a class="navbar-brand glyphicon glyphicon-home" href="index.jsp"></a>
                    <a class="navbar-brand" id="selected" href="${seeRooms}">Rooms</a>
                    <a class="navbar-brand" href="contact.jsp">Contact</a>
                    <a class="navbar-brand" href="login.jsp">Log in</a>
                    <a class="navbar-brand" href="register.jsp">Register</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse navbar-right">
                    <p class="navbar-brand">username: ${USER.username}  ,   role: ${USER.role}</p>
                </div>
            </div>
        </nav>
        <ol>
            <li><a href="rooms/room1.html"> Room 1</a></li>
            <a target="_blank" href="rooms/room1.html">
                <img src="img/room1/bed.jpg" alt="Room1" width="600" height="400">
            </a>
            <br>
            <table style="width:100%">
                <tr>
                    <th>Price</th>
                    <th>Number of beds</th>
                    <th>Kitchen</th>
                    <th>Bathroom</th>
                    <th>Terrace</th>
                </tr>
                <tr>
                    <td>150$</td>
                    <td>2</td>
                    <td>YES</td>
                    <td>YES</td>
                    <td>YES</td>
                </tr>
            </table>
            <br>

            <li><a href="rooms/room2.html"> Room 2</a></li>
            <a target="_blank" href="rooms/room2.html">
                <img src="img/room2/bed.jpg" alt="Room2" width="600" height="400">
            </a>
            <br>
            <table style="width:100%">
                <tr>
                    <th>Price</th>
                    <th>Number of beds</th>
                    <th>Kitchen</th>
                    <th>Bathroom</th>
                    <th>Terrace</th>
                </tr>
                <tr>
                    <td>180$</td>
                    <td>3</td>
                    <td>YES</td>
                    <td>YES</td>
                    <td>YES</td>
                </tr>
            </table>
            <br>

            <li><a href="rooms/room3.html"> Room 3</a></li>
            <a target="_blank" href="rooms/room3.html">
                <img src="img/room3/bed.jpg" alt="Room3" width="600" height="400">
            </a>
            <br>
            <table style="width:100%">
                <tr>
                    <th>Price</th>
                    <th>Number of beds</th>
                    <th>Kitchen</th>
                    <th>Bathroom</th>
                    <th>Terrace</th>
                </tr>
                <tr>
                    <td>100$</td>
                    <td>1</td>
                    <td>YES</td>
                    <td>YES</td>
                    <td>YES</td>
                </tr>
            </table>
            <br>
        </ol>
    </body>
</html>
