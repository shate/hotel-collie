create database hotelCollie;
use hotelCollie;

drop table reservations; drop table customers; drop table rooms; drop table hotels; drop table users;

drop table users;

create table users(
id int primary key auto_increment,
username varchar(40),
password varchar(40),
role varchar(10)
);

insert into users(username, password, role)
values('client', 'client', 'CLIENT'), ('admin', 'admin', 'ADMIN');



create table hotels(
id int primary key auto_increment,
name varchar (40),
street varchar (30),
zip_code varchar (6),
city varchar (30),
website varchar(100)
);

insert into hotels(name, street, zip_code, city, website)
values ('Collie', 'Borderowa', '51-510', 'Wrocław', 'www.bcollie.com');


create table rooms(
id int primary key auto_increment,
type varchar (40),
capacity int,
price double,
available int,
amenities varchar (200)
);

insert into rooms(type, capacity, price, available, amenities)
values('base', 2, 150, 4, 'kitchen, bathroom'), ('comfort', 3, 250, 3, 'kitchen, bathroom, terrace, dogs'), 
('premium', 4, 350, 2, 'kitchen, bathroom, terrace, dogs, sauna');
        
create table customers(
id int primary key auto_increment,
pesel varchar (11),
first_name varchar (30),
surname varchar (30),
email varchar (40)
);

insert into customers(pesel, first_name, surname, email)
values('11223344556', 'Jon', 'Snow', 'knownothing@raven.com');
        
        
create table reservations(
id int primary key auto_increment,
customerID int,
roomID int,
start_date date,
end_date date,
days int,
foreign key (customerID) references customers(id),
foreign key (roomID) references rooms(id)
);

insert into reservations(customerID, roomID, start_date, end_date)
values(1, 1, '2020-05-02', '2020-05-05');


delimiter $$
create procedure book(customerID int, roomID int, start_date date, end_date date)
	begin
	declare days int;
	declare availableRooms int;
	set days = datediff(end_date, start_date);
	set availableRooms = (select available from rooms where id = roomID);
	if(availableRooms > 0) then
		insert into reservations(customerID, roomID, start_date, end_date, days) values(1, 1, start_date, end_date, days);
		set availableRooms = availableRooms - 1;
		update rooms set available = availableRooms where id = roomID;
	end if;
end $$
delimiter ; 

delimiter $$
create procedure deleteReservation(reservationID int)
	begin  
    if(reservationID  in (select id from reservations)) then
		delete from reservations where id = reservationID;
    end if;
end $$
delimiter ; 


select * from reservations;
select * from users;

call book(1, 1, '2020-05-10', '2020-05-15');
CALL book(1, 1, '2020-04-26', '2020-04-28');
call deleteReservation(4);

SELECT role FROM users WHERE username = 'customer' and password = 'customer'
    
    
    


        
        